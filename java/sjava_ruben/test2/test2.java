import java.util.Map;
import java.util.TreeMap;
import java.until.Map.Entry;

import com.sun.xml.internal.bind.v2.util.QNameMap.Entry;

class test2{

    public static void main(String[] args) {

        Map<Integer,String> motos = new TreeMap<Integer,String>();

        motos.put(1, "Honda");
        motos.put(2, "Kawasaki");
        motos.put(3, "Suzuki");
        motos.put(4, "Vespa");

        for (Entry e : motos.entrySet()) {
            System.out.printf("La marca %s tiene el indice %d\n", e.getValue(), e.getKey());
        };
    }

}