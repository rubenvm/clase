import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MaximizeAction;

class InfoNums {
    
    public static void main(String[] numeros) {
        int max=Integer.parseInt(numeros[0]);
        int min=Integer.parseInt(numeros[0]);
        int sum=0;
        int med=0;
        for (String s : numeros){
            int num = Integer.parseInt(s);
            System.out.print(num+ "\t");

            if (num>max) 
                max=num;
            
            if (num<min)
                min=num;
            sum += num;
            med=sum/numeros.length;
        }
        System.out.println();
        System.out.println("El maximo es "+max);
        System.out.println("El minimo es "+min);
        System.out.println("La media es "+med);
    }
}
